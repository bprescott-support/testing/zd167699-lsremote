#!/bin/sh

repolist=$(dirname $0)/repos.list
repostate=$(dirname $0)/repos.state
statewip=$(dirname $0)/repos.state_WIP
repotmp=$(dirname $0)/repos.tmp
  # how long to sleep between loops
sleepv=3

  # error handling
  # need repo list

if [ ! -f $repolist ]; then
  echo "$(date --iso-8601=sec) ${repolist} missing"
  exit 1
fi

  # need git binary

if gitb=$(which git); then
  :
elif [ -x "/opt/gitlab/embedded/bin/git" ]; then
  gitb="/opt/gitlab/embedded/bin/git"
else
  echo "$(date --iso-8601=sec) cannot find git binary"
  exit 1
fi

if [ ! -x $gitb ]; then
  echo "$(date --iso-8601=sec) bug in code locating git binary"
  exit 1
fi

  # loop forever

while true ; do

  if [ ! -f $repostate ]; then
    # first run
    first="true"
  else
    first="false"
  fi

  [ -f $statewip ] && rm -f $statewip

  egrep -v '^$|^#' ${repolist} | while read repo ; do
#      # tidy up repo name
#    nice=$(echo "$repo" | awk -F: '{ print $2 }' | sed 's~.git$~~g')
    nice="${repo}"
      # if there is no state at all, set -1 as previous count
    if [ "true" = "$first" ]; then
      was="-1"
    else
      was=$(grep "${nice}" ${repostate} | awk -F, '{ print $2 }')
        # new repos won't have state
      if [ "x" = "x$was" ]; then
        was="-1"
      fi
    fi

    now=$(date --iso-8601=sec)
    if $gitb ls-remote -h -- "${repo}" > ${repotmp} 2>/dev/null ; then

      count=$(wc -l ${repotmp} | awk '{ print $1 }')
      rm -f ${repotmp}
      if [ 0 -eq $count ]; then
        echo "${now} ${nice} count=0 was=${was}"
          # keep previous state; no point logging zero
        echo "${nice},${was},zero" >> ${statewip}
      else
        echo "${nice},${count},ok" >> ${statewip}
      fi
    else
        # keep previous state; cannot trust erroring git
      echo "${nice},${was},error" >> ${statewip}
    fi
  done

  mv $statewip $repostate
  sleep ${sleepv}

done
