# Introduction

Script for polling a list of repositories.

It counts branches returned by `git ls-remote -h` and logs when they drop to zero.

Tested using SSH git clone addresses, not HTTPS.

# Setup

Recommend running this on a GitLab server. The embedded Omnibus git binary will be use if one isn't in the PATH.

1. Clone the repo using the account which will run the script. The account needs write access to the local disk where the repo is cloned.
   - The account will require an SSH key that gives it access to the repositories you want to poll.
2. Create and populate `repos.list` alongside the script with a list of SSH clone URLs
   - `.gitignore` will allow you (and the script) to create required certain files within your checked out copy.
   - The script is coded to expect required files to be co-located with itself.

```
git@git.example.com:some/path/repo.git
git@git.example.com:someother/path/repo.git
```

# Run script

- Run with `nohup` and in the background, and then you can log off - it'll carry on in the background.
- Script has an infinite loop in it.
- Kill the process when you don't want it running any more.

```
cd /path/to/cloned/repo
nohup ./pollrepo.sh >> ./pollrepo_output.log 2>&1 &
```

- It'll track state in `repos.state` including branch count
- Add more repos to `repos.list` as needed, no need to restart the process.
- Log entries will be written when it sees issues with repositories.

# Potential future enhancements

- Recheck a repository if it drops to zero; how transitory is it?
- Time how long the `git ls-remote` runs for.
- Query the [branches API](https://docs.gitlab.com/ee/api/branches.html#list-repository-branches) and see if it gets the same result.
- Creation of a specific file will cause the daemonised script to shut down.
- Might be useful to log when a repo recovers, if the hypothesis that volumes of branch changes triggers the issue
